<?php

namespace App\Form;

use App\Entity\Superheros;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class SuperherosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nickname')
            ->add('real_name')
            ->add('origin_description')
            ->add('superpowers')
            ->add('catch_phrase')
            ->add('imagesFile', FileType::class, [
                'mapped' => false,
                'constraints' => [
                    new Image(),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Superheros::class,
        ]);
    }
}
