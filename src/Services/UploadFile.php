<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadFile
{
    /**
     * @var string
     */
    private $uploadsPath;

    /**
     * UploadFile constructor.
     */
    public function __construct(string $uploadPath)
    {
        $this->uploadsPath = $uploadPath;
    }

    public function uploadImage(UploadedFile $file)
    {
        $path = $this->uploadsPath;
        $nameFile = $file->getClientOriginalName();
        $file->move($path, $nameFile);

        return $nameFile;
    }
}
