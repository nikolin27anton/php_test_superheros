<?php

namespace App\Entity;

use App\Repository\SuperherosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SuperherosRepository::class)
 */
class Superheros
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nickname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $real_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $origin_description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $catch_phrase;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $images;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $superpowers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getRealName(): ?string
    {
        return $this->real_name;
    }

    public function setRealName(string $real_name): self
    {
        $this->real_name = $real_name;

        return $this;
    }

    public function getOriginDescription(): ?string
    {
        return $this->origin_description;
    }

    public function setOriginDescription(string $origin_description): self
    {
        $this->origin_description = $origin_description;

        return $this;
    }

    public function getCatchPhrase(): ?string
    {
        return $this->catch_phrase;
    }

    public function setCatchPhrase(string $catch_phrase): self
    {
        $this->catch_phrase = $catch_phrase;

        return $this;
    }

    public function getImages(): ?string
    {
        return '/uploads/'.$this->images;
    }

    public function setImages(string $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getSuperpowers(): ?string
    {
        return $this->superpowers;
    }

    public function setSuperpowers(string $superpowers): self
    {
        $this->superpowers = $superpowers;

        return $this;
    }
}
