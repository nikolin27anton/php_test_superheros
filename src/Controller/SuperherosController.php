<?php

namespace App\Controller;

use App\Entity\Superheros;
use App\Form\SuperherosType;
use App\Repository\SuperherosRepository;
use App\Services\UploadFile;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/superheros")
 */
class SuperherosController extends AbstractController
{
    /**
     * @Route("/", name="superheros_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
//        return $this->render('superheros/index.html.twig', [
//            'superheros' => $superherosRepository->findAll(),
//        ]);
        $query = $this->getDoctrine()
            ->getRepository(Superheros::class)
            ->createQueryBuilder('c');

        $superhero = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $request->getSession()->get('items', $request->query->get('items', 5))
        );

        return $this->render('superheros/index.html.twig', ['superheros' => $superhero]);

    }

    /**
     * @Route("/new", name="superheros_new", methods={"GET","POST"})
     */
    public function new(Request $request, UploadFile $uploadFile): Response
    {
        $superhero = new Superheros();
        $form = $this->createForm(SuperherosType::class, $superhero);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['imagesFile']->getData();
            $nameFile = $uploadFile->uploadImage($file);
            $superhero->setImages($nameFile);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($superhero);
            $entityManager->flush();

            return $this->redirectToRoute('superheros_index');
        }

        return $this->render('superheros/new.html.twig', [
            'superhero' => $superhero,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="superheros_show", methods={"GET"})
     */
    public function show(Superheros $superhero): Response
    {
        return $this->render('superheros/show.html.twig', [
            'superhero' => $superhero,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="superheros_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Superheros $superhero, UploadFile $uploadFile): Response
    {
        $form = $this->createForm(SuperherosType::class, $superhero);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['imagesFile']->getData();
            $nameFile = $uploadFile->uploadImage($file);
            $superhero->setImages($nameFile);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('superheros_index');
        }

        return $this->render('superheros/edit.html.twig', [
            'superhero' => $superhero,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="superheros_delete", methods={"POST"})
     */
    public function delete(Request $request, Superheros $superhero): Response
    {
        if ($this->isCsrfTokenValid('delete'.$superhero->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($superhero);
            $entityManager->flush();
        }

        return $this->redirectToRoute('superheros_index');
    }
}
